const express = require('express');
const app = express();

const PORT = process.env.PORT || 3000

app.get('/', (req, res) => {
  res.send('Hello World 1.3');
});

app.listen(PORT, () => {
  console.log(`Aplicação rodando na porta ${PORT}!`);
});